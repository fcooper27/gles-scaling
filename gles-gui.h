#ifndef __GUI_H__
#define __GUI_H__


#include <stdint.h>

// AWH #include "nes/driver.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

struct SDL_Surface;

extern SDL_Surface *screen1024, *screen512, *screen256;
extern void *tex256buffer, *tex512buffer;
extern int guiQuit;
extern int texToUse;


extern int doGuiSetup(void);
extern int doGui(void);


extern int emuDone;

/* eglSetup.c */
extern void EGLShutdown(void);
extern void EGLFlip(void);
extern int EGLInitialize(void);

/* eglTexture.c */
enum {
  TEXTURE_256 = 0,
  TEXTURE_512 = 1,
  TEXTURE_1024 = 2,
  TEXTURE_PAUSE = 3,
  TEXTURE_SCREENSHOT = 4,
  TEXTURE_LAST = 5
};

extern void EGLSrcSize(unsigned int width, unsigned int height);
extern void EGLDestSize(unsigned int width, unsigned int height);
extern void EGLSetupGL(void);
extern void EGLBlitGL(void *buf,int pitch);
extern void EGLBlitGLCache(void *buf, int cache);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GUI_H__ */

