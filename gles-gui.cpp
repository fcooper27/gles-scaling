
#include <stdlib.h>

#include <SDL.h>
#include "gles-gui.h"

SDL_Surface *screen256, *screen512;
static SDL_Surface *screen;
SDL_Surface *fbscreen;

static void quit(int rc)
{
  SDL_Quit();
  exit(rc);
}

int doGuiSetup(void)
{
  /* Initialize SDL */
  if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0 ) {
    fprintf(stderr, "Couldn't initialize SDL: %s\n",SDL_GetError());
    return(1);
  }


  /* Set video mode */
#if defined(PC_PLATFORM)
  /* Determines the physical window size */
  if ( (fbscreen = SDL_SetVideoMode(/*720, 480,*/320,240, 16, 0)) == NULL ) {
    fprintf(stderr, "Couldn't set 720x480x16 video mode: %s\n",
      SDL_GetError());
    quit(2);
  }
#else
  if ( (fbscreen = SDL_SetVideoMode(0, 0, 16, 0)) == NULL) {
    fprintf(stderr, "Couldn't set native 16 BPP video mode: %s\n",
      SDL_GetError());
    quit(2);
  }
#endif /* PC_PLATFORM */

  screen512 = SDL_CreateRGBSurface(0, 512, 512,
    16, fbscreen->format->Rmask, fbscreen->format->Gmask,
    fbscreen->format->Bmask, fbscreen->format->Amask);
  screen256 = SDL_CreateRGBSurface(0, 256, 256,
    16, fbscreen->format->Rmask, fbscreen->format->Gmask,
    fbscreen->format->Bmask, fbscreen->format->Amask);
  screen = screen512;

  tex256buffer = calloc(1, 256*256*2);
  tex512buffer = calloc(1, 512*512*2);

  /* Shut off the mouse pointer */
  SDL_ShowCursor(SDL_DISABLE);

  /* Turn on EGL */
  EGLInitialize();
  EGLDestSize(fbscreen->w, fbscreen->h);
  EGLSrcSize(512, 512);

  return 0;
}
