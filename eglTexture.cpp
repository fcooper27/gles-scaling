#include <stdint.h>
#include <stdlib.h>
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <SDL.h>
#include "gles-gui.h"


void *tex256buffer = NULL;
void *tex512buffer = NULL;

extern SDL_Surface *fbscreen;

static GLuint textures[TEXTURE_LAST]={0,0,0,0,0};
/* C, D, A, A, D, B */
static GLfloat mainTexCoords[18];
static GLfloat mainTexPosCoords[12] = {
  -1.0f, -1.0f, 
   1.0f, -1.0f, 
  -1.0f,  1.0f, 
  -1.0f,  1.0f, 
   1.0f, -1.0f,
   1.0f,  1.0f }; 


int texToUse = TEXTURE_256;
static float srcWidth = 1.0f;
static float srcHeight = 1.0f;
static float destScreenWidth = 1.0f;
static float destScreenHeight = 1.0f;
static float destAspectRatio = 1.0f;
static float srcAspectRatio = 1.0f;
static float scaleWidth = 1.0f;
static float scaleHeight = 1.0f;
static float translateX = -1.0f;
static float translateY = -1.0f;


static void setupMainTexCoords(void)
{
  /* C */
  mainTexCoords[0] = -1.0f;
  mainTexCoords[1] = 1.0f;
  mainTexCoords[2] = 0.0f;

  /* D */
  mainTexCoords[3] = 1.0f;
  mainTexCoords[4] = 1.0f;
  mainTexCoords[5] = 0.0f;

  /* A */
  mainTexCoords[6] = -1.0f;
  mainTexCoords[7] = -1.0f;
  mainTexCoords[8] = 0.0f;

  /* A */
  mainTexCoords[9] = -1.0f;
  mainTexCoords[10] = -1.0f;
  mainTexCoords[11] = 0.0f;

  /* D */
  mainTexCoords[12] = 1.0f;
  mainTexCoords[13] = 1.0f;
  mainTexCoords[14] = 0.0f;

  /* B */
  mainTexCoords[15] = 1.0f;
  mainTexCoords[16] = -1.0f; 
  mainTexCoords[17] = 0.0f;
}

void setupMainTexRefCoords(void)
{
  float left, right, top, bottom;
  int texSize = 256 << texToUse;
  left = -1.0f * (srcWidth / texSize);
  right = 1.0f * (srcWidth / texSize);
  top = 1.0f * (srcHeight / texSize);
  bottom = -1.0f * (srcHeight / texSize);

  mainTexPosCoords[0] = left;
  mainTexPosCoords[1] = bottom;
  mainTexPosCoords[2] = right;
  mainTexPosCoords[3] = bottom;
  mainTexPosCoords[4] = left;
  mainTexPosCoords[5] = top;
  mainTexPosCoords[6] = left;
  mainTexPosCoords[7] = top;
  mainTexPosCoords[8] = right;
  mainTexPosCoords[9] = bottom; 
  mainTexPosCoords[10] = right;
  mainTexPosCoords[11] = top;
}

void EGLBlitGL(void *buf,int pitch) {



    EGLBlitGLCache(buf,0);

}

void EGLBlitGLCache(void *buf, int cache)
{

    unsigned char * mybuf = (unsigned char *)buf;
    int width = srcWidth;
    int height = srcHeight;


    /* Do a little black magic
     * Since the texture is smaller than the screen size
     * value for GL_TEXTURE_WRAP_S/T becomes very important. However,
     * all the apporiate values are ugly. Examples can be seen here:
     * http://gamedev.stackexchange.com/questions/62548/what-does-changing-gl-texture-wrap-s-t-do
     * The best option is to use clamping but that ends up repeating the first pixel
     * in each row for the amount we translated for. As a workaround to maintain a nice
     * black border. Set the very first pixel to black which will result in the black
     * pixel being repeated. Which indirectly is what we really want to happen in the first place
     */
    int dstRowBytes = 512;
	width <<= 1;
    
	for (; height; height--)
	{
        mybuf[0] = 0;
        mybuf[1] = 0;
		mybuf += dstRowBytes;
	}


  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  switch(texToUse) {
    case TEXTURE_256:
      glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_256]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256, 0,
        GL_RGB, GL_UNSIGNED_SHORT_5_6_5, buf);
      if (cache)
        memcpy(tex256buffer, buf, 256 * 256 * 2);
      break;
    case TEXTURE_512:
      glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_512]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 512, 512, 0,
        GL_RGB, GL_UNSIGNED_SHORT_5_6_5, buf);
      if (cache)
        memcpy(tex512buffer, buf, 512 * 512 * 2);
      break;
    case TEXTURE_1024:
    default:
      glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_1024]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024, 0,
        GL_RGB, GL_UNSIGNED_SHORT_5_6_5, buf);
      break;
  }

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glPushMatrix();
  glTranslatef(translateX, translateY, 0.0f);
  glScalef(scaleWidth,scaleHeight, 1.0f);
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glFrontFace(GL_CW);
  glVertexPointer(3, GL_FLOAT, 0,mainTexCoords);
  glTexCoordPointer(2, GL_FLOAT, 0, mainTexPosCoords);
  glDrawArrays(GL_TRIANGLES,0,6);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glPopMatrix();

}

void EGLSetupGL(void)
{
  int i;

  setupMainTexCoords();
  glGenTextures(TEXTURE_LAST, textures);

  for (i=0; i < TEXTURE_LAST; i++)
  {
    glBindTexture(GL_TEXTURE_2D, textures[i]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
  }

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glDisable(GL_DITHER);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Background color to red
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void EGLSrcSize(unsigned int width, unsigned int height)
{
  float heightTweak = 1.0f;
  float widthTweak = 1.0f;
  float temp,temp2,x,y;

  srcWidth = (float)width;
  srcHeight = (float)height;
  srcAspectRatio = srcWidth / srcHeight;

  /* Determine the texture to render to */
  if ((width <= 256) && (height <= 256)) {
    texToUse = TEXTURE_256;
    memset(tex256buffer, 0x0, 256 * 256 * 2); 
  }
  else if ((width <= 512) && (height <= 512)) {
    texToUse = TEXTURE_512;
    memset(tex512buffer, 0x0, 512 * 512 * 2);
  }
  else
    texToUse = TEXTURE_1024;

  /* Scale to fit screen while maintain aspect ratio */

  temp = destScreenWidth/srcWidth;
  temp2 = destScreenHeight/srcHeight;
  if(temp*srcHeight > destScreenHeight)
    scaleWidth = scaleHeight = temp2;
  else
    scaleWidth = scaleHeight = temp;

  /*                    Center screen image
   *  Translation of 0,0 means the upper left corner of
   *  the image(texture) being displayed will be in the center of the
   *  screen. -1,1 means the upper left corner of the image will be displayed
   *  in the upper left corner of the screen as you would expect. With that
   *  knowledge and a bit of math we can calculate how much we need to shift the
   *  output to fit the middle of the screen
   */
     
  x = 1/(destScreenWidth/2);
  y = (scaleWidth*srcWidth)/2*x;
  translateX = -1.0f*y;

  x = 1/(destScreenHeight/2);
  y = (scaleHeight*srcHeight)/2*x;
  translateY = 1.0f*y;
   

  /* Some reason the output is streched to half the screen's (dest) width and
   * height. So scale the output to match the proper srcwidth and srcheight
   * otherwise our previous calculations will "appear" off.
   */

  widthTweak = srcWidth/(destScreenWidth/2);
  heightTweak = srcHeight/(destScreenHeight/2);
  scaleWidth *= widthTweak;
  scaleHeight *=  heightTweak;

  setupMainTexRefCoords();
}

void EGLDestSize(unsigned int width, unsigned int height)
{
  printf("EGLDestSize width %d height %d\n",width,height);
  destScreenWidth = (float)width;
  destScreenHeight = (float)height;
  destAspectRatio = destScreenWidth / destScreenHeight;


}

